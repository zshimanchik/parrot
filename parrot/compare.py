import numpy as np
from scipy.io.wavfile import read
from spafe.features.mfcc import mfcc
from spafe.utils.preprocessing import SlidingWindow
from spafe.utils.vis import show_features


def get_mfcc_from_file(fpath):
    fs, sig = read(fpath)

    # compute mfccs and mfes
    mfccs  = mfcc(sig,
                fs=fs,
                pre_emph=1,
                pre_emph_coeff=0.97,
                window=SlidingWindow(0.03, 0.015, "hamming"),
                nfilts=128,
                nfft=2048,
                low_freq=0,
                high_freq=8000,
                normalize="mvn")
    return mfccs

def compare_files(file1, file2):
    mfccs1 = get_mfcc_from_file(file1)
    mfccs2 = get_mfcc_from_file(file2)
    file1_length = mfccs1.shape[0]
    file2_length = mfccs2.shape[0]

    min_length = min(mfccs1.shape[0], mfccs2.shape[0])
    mfccs1.resize((min_length, mfccs1.shape[1]))
    mfccs2.resize((min_length, mfccs2.shape[1]))
    diff = np.mean(np.abs(mfccs1 - mfccs2))

    return diff, file1_length, file2_length


def compare_in_dir(directory):
    import os

    import matplotlib.pyplot as plt
    files = os.listdir(directory)

    matr = []
    for file1 in files:
        matr.append([])
        for file2 in files:
            os.path.join(directory, file1)
            diff, _, _ = compare_files(os.path.join(directory, file1), os.path.join(directory, file2))
            matr[-1].append(diff)

    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.matshow(matr)
    ax.set_xticklabels(['']+files)
    ax.set_yticklabels(['']+files)

    print(files)
    plt.show()


def visualize(fpath):
    mfccs = get_mfcc_from_file(fpath)
    show_features(mfccs, f"{fpath}", "MFCC Index", "Frame Index")
