import datetime
import os
import random
import struct
import time
import wave

import pyaudio
import pydub

import compare

# Define parameters
FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = 44100
CHUNK = 2048
RECORD_SECONDS = 10
SILENCE_ENERGY_THRESHOLD = 450
SILENCE_LENGTH = 1

DIFF_THRESHOLD = 0.25

WHISTLES_FOLDER = 'whistles'

# Initialize PyAudio
audio = pyaudio.PyAudio()

out_stream = audio.open(format=FORMAT,
                    channels=CHANNELS,
                    rate=RATE,
                    output=True)

silence_start = None

# if folder 'recordings' does't exist, create it
os.makedirs('recordings', exist_ok=True)

def save_audio(data, filename):
    with wave.open(filename, 'wb') as wf:
        wf.setnchannels(CHANNELS)
        wf.setsampwidth(audio.get_sample_size(FORMAT))
        wf.setframerate(RATE)
        wf.writeframes(b"".join(data))

def shift_pitch(data, octaves=0.5):
    sound = pydub.AudioSegment(
        data=data,
        sample_width=2,
        frame_rate=RATE,
        channels=CHANNELS
    )

    new_sample_rate = int(sound.frame_rate * (2.0 ** octaves))
    hipitch_sound = sound._spawn(sound.raw_data, overrides={'frame_rate': new_sample_rate})
    hipitch_sound = hipitch_sound.set_frame_rate(RATE)
    return hipitch_sound.raw_data

def get_random_whistle():
    filename = random.choice([x for x in os.listdir(WHISTLES_FOLDER) if x.endswith('.wav')])
    with wave.open(os.path.join(WHISTLES_FOLDER, filename), "rb") as wf:
        nframes = wf.getnframes()
        data = wf.readframes(nframes)
    return data


while True:
    # Open audio stream
    in_stream = audio.open(format=FORMAT,
                        channels=CHANNELS,
                        rate=RATE,
                        input=True,
                        frames_per_buffer=CHUNK)

    # Record audio
    audio_buffer = []
    # for i in range(0, int(RATE / CHUNK * RECORD_SECONDS)):
    while len(audio_buffer) < int(RATE / CHUNK * RECORD_SECONDS):
        data = in_stream.read(CHUNK)
        
        samples = struct.unpack(f"{CHUNK}h", data)
        energy = sum([abs(x) for x in samples]) / len(samples)
        is_silence = energy < SILENCE_ENERGY_THRESHOLD
        silence_for = time.time() - silence_start if silence_start else 0

        # print(f'inloop {silence_for=:.2f} {len(audio_buffer)=} {energy=:.1f}')

        if len(audio_buffer) == 0 and is_silence:
            continue

        print(f'inloop {silence_for=:.2f} {len(audio_buffer)=} {energy=:.1f}')
        audio_buffer.append(data)

        if is_silence:
            if silence_start is None:
                silence_start = time.time()
            
            if time.time() - silence_start > SILENCE_LENGTH:
                break
        else:
            silence_start = None


    # Stop recording audio
    in_stream.stop_stream()
    in_stream.close()
    silence_start = None

    # delete silence at the end
    audio_buffer = audio_buffer[:-int(RATE / CHUNK * SILENCE_LENGTH)]

    save_audio(audio_buffer, 'audio.wav')

    # Searching for similar audio files in recordings
    diffs = []
    print('=='*8)
    for file in os.listdir('recordings'):
        if file.endswith('.wav'):
            diff, f1_length, f2_length = compare.compare_files('audio.wav', f'recordings/{file}')
            diffs.append((file, diff, f1_length, f2_length))
    diffs.sort(key=lambda x: x[1])

    matching = []
    for file, diff, f1_length, f2_length in diffs:
        print(f'{file:<25} {diff=:.2f} {f1_length=} {f2_length=}')
        # its match if first audio is shorter
        if diff < DIFF_THRESHOLD and f1_length * 1.5 < f2_length:
            matching.append((file, diff))
    
    if not matching:
        print('No matches. Adding new audio then')
        now = datetime.datetime.now().strftime('%Y-%m-%dT%H-%M-%S')
        filename = f'recordings/{now}.wav'
        print(f'saving {filename}')
        save_audio(audio_buffer, filename)
        data = get_random_whistle()
        out_stream.write(data)
    else:
        print(f'choises {matching}')
        best_match = min(matching, key=lambda x: x[1])
        file = best_match[0]
        # file = random.choice(matching)
        with wave.open(os.path.join('recordings', file), "rb") as wf:
            # Extract audio data and parameters
            nframes = wf.getnframes()
            # Read audio data as bytes
            data = wf.readframes(nframes)

        # trim audio with length of audio_buffer, so it will "continue" the phrase
        audio_buffer_len = sum(len(x) for x in audio_buffer)
        print(f'{audio_buffer_len=} {len(data)=}')
        data = data[audio_buffer_len:]
        data = shift_pitch(data, octaves=0.4)
        out_stream.write(data)


# Cleanup
# out_stream.stop_stream()
# out_stream.close()
