import wave
import matplotlib.pyplot as plt
import numpy as np

# Open WAV file
filename = "recordings/2023-03-05T20-34-56.wav"
with wave.open(filename, "rb") as wf:
    # Extract audio data and parameters
    nframes = wf.getnframes()
    framerate = wf.getframerate()
    nchannels = wf.getnchannels()
    sampwidth = wf.getsampwidth()

    # Read audio data as bytes
    data = wf.readframes(nframes)

    # Convert bytes to numpy array of integers
    if sampwidth == 2:
        # If each sample is 16 bits (2 bytes), interpret as int16
        data_int = np.frombuffer(data, dtype=np.int16)
    elif sampwidth == 1:
        # If each sample is 8 bits (1 byte), interpret as uint8
        data_int = np.frombuffer(data, dtype=np.uint8)
        # Convert from unsigned to signed
        data_int = (data_int - 128).astype(np.int8)

    # Reshape array to separate left and right channels
    if nchannels == 2:
        data_int = data_int.reshape(-1, 2)

# Plot audio waveform
time = np.arange(0, nframes) / framerate
fig, ax = plt.subplots()
ax.plot(time, data_int)
ax.set(xlabel="Time (s)", ylabel="Amplitude", title="Audio waveform")
plt.show()